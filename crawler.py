import logging

import pandas as pd
import requests
from bs4 import BeautifulSoup
from dateutil.parser import parse

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M:%S', filename='./log.log')

logger = logging.getLogger(__name__)


# TODO use thread instead of open 5 terminator
class CrawlerClient:

    def __init__(self, input_file, output_file):
        self.input_file = input_file
        self.output_file = output_file
        self.hotel_name = ''
        self.details = ''
        self.locality = ''
        self.country = ''
        self.rating = -1
        self.rating_date = 'N/A'
        self.handy_list = []
        self.span_rating = ''
        self.avg_rating = -1
        self.comment = ''
        self.title = ''
        self.link_list = []
        self.max_page_num = -1
        self.domain = 'https://en.tripadvisor.com.hk'

    # TODO can set 5 list instead of 5 csv
    def get_handy_data(self):
        with open(self.input_file, 'rb') as f:
            for i in f.readlines():
                split = i.decode().strip().split(',')
                self.handy_list.append(split)

    # TODO to avoid extract more time, but soup can't be set in self(no soup type)
    def get_html_content(daself, url):
        try:
            res = requests.get(url)
        except Exception as e:
            pass
        else:
            html_content = res.text
            soup = BeautifulSoup(html_content, 'lxml')
            return soup

    def get_page_num(self, soup):
        link_list = []
        num_list = []

        soup = soup.find(id='REVIEWS')
        page_nums = soup.find_all("div", class_='pageNumbers')
        for page_num in page_nums:
            aa = page_num.find_all('a')
            for a in aa:
                link = a.get('href')
                off_set = a.get('data-page-number')
                if isinstance(link, str):
                    num_list.append(off_set)
                    if int(off_set) == 1:
                        link_list = link.split('Reviews')
        # offset =  5 * (num-1)
        try:
            last = int(num_list[-1])
        except Exception as e:
            pass
        else:
            self.link_list = link_list
            self.max_page_num = last

    def get_common_info(self, soup):
        # location, hotel name, total rating
        try:
            self.hotel_name = soup.find(id="HEADING").get_text()
        except Exception as e:
            logger.error('cant find hotel name {}'.format(e))

        try:
            self.details = soup.find_all('span', class_='detail')
        except Exception as e:
            logger.error('cant find location {}'.format(e))
        else:
            for detail in self.details:
                try:
                    self.locality = detail.select('span.locality')[0].get_text()
                except Exception as e:
                    pass
                try:
                    self.country = detail.select('span.country-name')[0].get_text()
                except Exception as e:
                    pass
        try:
            a = soup.find_all('div', class_='prw_rup prw_common_bubble_rating rating')
        except Exception as e:
            logger.error('cant find avg_rating {}'.format(e))
        else:
            try:
                self.span_rating = a[0].find('span', class_='ui_bubble_rating')
            except Exception as e:
                pass
            try:
                self.span_rating = self.span_rating.get('class')
            except Exception as e:
                pass
        try:
            self.avg_rating = int(self.span_rating[1].split('_')[1])
        except Exception as e:
            logger.error('index error {}'.format(e))

        # def get_comment_info(self, soup):
        '''
        get comment(more) selenium
        <span class="taLnk ulBlueLinks" onclick="widgetEvCall('handlers.clickExpand',event,this);">More</span>
        or
        <a href="/ShowUserReviews-g1010802-d8754256-r626006700-SOULMADE-Garching_bei_Munchen_Upper_Bavaria_Bavaria.html" class="title " onclick="(ta.prwidgets.getjs(this,'handlers')).reviewClick(this.href, '0');" id="rn626006700"><span class="noQuotes">Lovely place in Munich</span></a>
        '''

    def check_date(self, soup, launch_date):
        print("check date")
        review_col_9 = soup.find_all('div', class_='ui_column is-9')
        # review_col_2 = soup.find_all('div', class_='ui_column is-2')

        for review in review_col_9:
            try:
                rating_date_span = review.select('span.ratingDate')[0]
            except IndexError as e:
                logger.error(e)
            else:
                try:
                    self.rating_date = rating_date_span.get('title').encode('utf-8').strip()
                except Exception as e:
                    pass
                else:
                    print(launch_date, parse(self.rating_date).date())
                    if launch_date > parse(self.rating_date).date():
                        return True
                    else:
                        return False

    def get_rating_info(self, soup):
        # result: list = [{rating, rating_date}, {rating, rating_date},....]
        result = []

        '''
        get all info
        <div class="ui_column is-9">...</div>
        '''
        # TODO can be in column container
        review_col_9 = soup.find_all('div', class_='ui_column is-9')
        # review_col_2 = soup.find_all('div', class_='ui_column is-2')

        for review in review_col_9:
            '''
            get rating example:
            <span class="ui_bubble_rating bubble_10"></span>
            '''
            try:
                rating_span = review.select('span')[0]
            except Exception as e:
                logger.info("Exception occurred", exc_info=True)
            else:
                try:
                    rating_list = rating_span.get('class')
                except Exception as e:
                    logger.error("Exception occurred", exc_info=True)
                else:
                    try:
                        self.rating = int(rating_list[1].split('_')[1])
                    except IndexError as e:
                        logger.error(e)

            # add comment
            try:
                self.comment = review.p.get_text().encode('utf-8').strip()
            except Exception as e:
                print('cant get comment')

            try:
                self.title = review.select('span.noQuotes')[0]
            except Exception as e:
                print('title not found')
            else:
                try:
                    self.title = self.title.get_text().encode('utf-8').strip()
                except Exception as e:
                    pass
            '''
            get date example:
            [<span class="ratingDate" title="31 October 2018">Reviewed 5 days ago </span>]
            '''
            try:
                rating_date_span = review.select('span.ratingDate')[0]
            except IndexError as e:
                logger.error(e)
            else:
                try:
                    self.rating_date = parse(rating_date_span.get('title').encode('utf-8').strip())
                except Exception as e:
                    pass

            if self.rating != -1 or self.rating_date != 'N/A':
                result.append(dict(rating=self.rating,
                                   rating_date=self.rating_date,
                                   comment=self.comment,
                                   title=self.title,
                                   ))

        return result

        # review_col_9 = soup.find_all('div', class_='ui_column is-9')
        # review_col_2 = soup.find_all('div', class_='ui_column is-2')
        # for review in review_col_9:
        #     try:
        #         comment = review.p.get_text()
        #     except Exception as e:
        #         print('cant get comment')
        #
        #     try:
        #         title = review.select('span.noQuotes')[0]
        #     except Exception as e:
        #         print('title not found')
        #     else:
        #         print(title.get_text())
        # for review in review_col_2:
        #     names = review.select('div.info_text')
        #     for name in names:
        #         try:
        #             a = name.find('div')
        #         except Exception as e:
        #             pass
        #         else:
        #             print(a.get_text())
        #     # print(name)

    # TODO all main can be grouped, do it in same time
    def main_common_info(self):
        self.get_handy_data()
        result = []
        for geo_id, destination_id, launch_date in self.handy_list[1:]:
            # info setting config
            geo_id = geo_id.split('"')[1]
            destination_id = destination_id.split('"')[1]
            url = 'https://en.tripadvisor.com.hk/Hotel_Review-{}-{}'.format(geo_id, destination_id)
            key = '{}-{}'.format(geo_id, destination_id)
            print(url)

            # process url, get html content
            soup = self.get_html_content(url)

            # extract useful information
            self.get_common_info(soup)
            result.append(dict(key=key,
                               hotel_name=self.hotel_name,
                               locality=self.locality,
                               country=self.country,
                               avg_rating=self.avg_rating,
                               ))

        df = pd.DataFrame(result)
        df.to_csv('./trip_advisor/common_info.csv')

    def main_rating_info(self):
        self.get_handy_data()

        df_total = pd.DataFrame()
        for geo_id, destination_id, launch_date in self.handy_list[1:]:
            # info setting config
            geo_id = geo_id.split('"')[1]
            destination_id = destination_id.split('"')[1]
            url = 'https://en.tripadvisor.com.hk/Hotel_Review-{}-{}'.format(geo_id, destination_id)
            key = '{}-{}'.format(geo_id, destination_id)
            print(url)

            # process url, get html content
            soup = self.get_html_content(url)

            # extract useful information
            rate_list = self.get_rating_info(soup)

            # the main DataFrame
            df = pd.DataFrame(rate_list)
            df['key'] = key
            df_total = pd.concat([df_total, df])
        try:
            df_total.to_csv('./trip_advisor/rating_info.csv', index=False)
        except IndexError as e:
            logger.error(e)

    def main_parse_page(self):

        self.get_handy_data()
        self.handy_list = [e for e in self.handy_list if e != None]

        df_total2 = pd.DataFrame()
        # TODO list first element is done in main_rating_info, combine later
        for geo_id, destination_id, launch_date in self.handy_list[1:]:
            # info setting config
            geo_id = geo_id.split('"')[1]
            destination_id = destination_id.split('"')[1]
            launch_date = launch_date.split('"')[1]
            url = 'https://en.tripadvisor.com.hk/Hotel_Review-{}-{}'.format(geo_id, destination_id)
            key = '{}-{}'.format(geo_id, destination_id)
            launch_date = parse(launch_date).date()

            print(url)

            # process url, get html content
            soup = self.get_html_content(url)
            if not soup:
                continue
            try:
                self.get_page_num(soup)
            except Exception as e:
                continue

            if self.max_page_num < 2 or len(self.link_list) != 2:
                continue
            df_total = pd.DataFrame()
            for i in range(2, self.max_page_num + 1):
                offset = 5 * (i - 1)
                link = '{}{}Reviews-or{}-{}'.format(self.domain, self.link_list[0], offset, self.link_list[1])
                print(link)

                # process url, get html content
                soup2 = self.get_html_content(link)

                # check date if older break
                aa = self.check_date(soup2, launch_date)
                print(aa)
                if aa:
                    break

                rate_list = self.get_rating_info(soup2)

                # the main DataFrame
                df = pd.DataFrame(rate_list)
                df['key'] = key
                df_total = pd.concat([df_total, df])

            df_total2 = pd.concat([df_total, df_total2])
        print(df_total2)
        try:
            df_total2.to_csv(self.output_file)
        except IndexError as e:
            logger.error(e)


if __name__ == "__main__":
    pass
    # CrawlerClient('./data_scientist_test.csv').main()
