from crawler import CrawlerClient

'''
test: data_scientist_test_2
'''


def common():
    CrawlerClient('./dataset/seperate_1500.csv', './trip_advisor/rating_info_1500.csv').main_common_info()


def rating():
    CrawlerClient('./dataset/seperate_1500.csv', './trip_advisor/rating_info_1500.csv').main_rating_info()


def add_on_rating():
    CrawlerClient('./dataset/data_scientist_test.csv', './trip_advisor/123.csv').main_parse_page()


add_on_rating()
