import time

# import googlemaps
import pandas as pd
import requests


def geo_analysis():
    GOOGLE_MAPS_API_URL = 'http://maps.googleapis.com/maps/api/geocode/json'
    params = {
        'address': '221B Baker Street, London, United Kingdom',
        'sensor': 'false',
        'region': 'uk'
    }

    # Do the request and get the response data
    req = requests.get(GOOGLE_MAPS_API_URL, params=params)
    res = req.json()

    print(res)
    # Use the first result
    result = res['results'][0]

    geodata = dict()
    geodata['lat'] = result['geometry']['location']['lat']
    geodata['lng'] = result['geometry']['location']['lng']
    geodata['address'] = result['formatted_address']

    print('{address}. (lat, lng) = ({lat}, {lng})'.format(**geodata))


def google_map(address):
    '''
    usage:
    gmaps = googlemaps.Client(key='Add Your Key here')
    # Geocoding an address
    geocode_result = gmaps.geocode('1600 Amphitheatre Parkway, Mountain View, CA')
    '''
    gmaps = googlemaps.Client(key='AIzaSyCqDGuTRZlW1qV10HqLdETH9zaj43OKS6c')
    geocode_result = gmaps.geocode(address)

    time.sleep(100)
    print(geocode_result)
    return geocode_result


if __name__ == "__main__":
    # df = pd.read_csv('./trip_advisor/common_info.csv', encoding='utf-8')

    df1 = pd.read_csv('./trip_advisor/rating_info.csv', encoding='utf-8')
    df2 = pd.read_csv('./trip_advisor/rating_info_500.csv', encoding='utf-8')
    df3 = pd.read_csv('./trip_advisor/rating_info_1000.csv', encoding='utf-8')
    df4 = pd.read_csv('./trip_advisor/rating_info_1500.csv', encoding='utf-8')
    df5 = pd.read_csv('./trip_advisor/rating_info_2000.csv', encoding='utf-8')
    df6 = pd.read_csv('./trip_advisor/rating_info_2500.csv', encoding='utf-8')

    df = pd.concat([df1, df2, df3, df4, df6])
    df_group = df.groupby(['key'], as_index=False)['rating'].mean()

    df2 = df1 = pd.read_csv('./trip_advisor/common_info.csv', encoding='utf-8')
    res = pd.merge(df2, df_group, on='key', how='left')
    print(res.loc[res.rating > res.avg_rating].count())
    # print(res[['key', 'rating', 'avg_rating']])
    # print(df.rating.astype(str).astype(float))
    # # 1. geocode (**no more free api now)
    # df['address'] = df.locality + df.country
    # print("start")
    # try:
    #     df['result'] = df['address'].apply(google_map)
    # except:
    #     pass
    # else:
    #     df.to_csv('./analysis/geo_code.csv')

    # 2. common statistic
    # total_rating = df.describe()['avg_rating']
    #
    # total_country_count = df.country.value_counts()
    #
    # print(df.hotel_name.value_counts())
